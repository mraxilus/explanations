[Explanations][github_pages]
===============================

_:speech_balloon: Providing interactive explanations of complex topics._


Purpose
-------
To explain various concepts and projects in an interactive and explorable fashion.
Some of these may be explanations may be heavily related to my own projects, and to that I say... I regret nothing.


License
-------
Copyright © Mr Axilus.
This project is licensed under [CC BY-NC-SA 4.0][license].

[github_pages]: http://mraxilus.github.io/explanations/
[license]: https://creativecommons.org/licenses/by-nc-sa/4.0/
